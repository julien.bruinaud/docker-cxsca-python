ARG CI_HOME

FROM python:3.9

# Install main dependencies
RUN apt-get -q update && \
    apt-get -q upgrade -y && \
    apt-get -q -y install ca-certificates zip wget && \
    apt-get -q clean

WORKDIR /opt/sca

# Download SCA Resolver

RUN wget https://sca-downloads.s3.amazonaws.com/cli/latest/ScaResolver-linux64.tar.gz -O ScaResolver-linux64.tar.gz && \
    tar -xzvf ScaResolver-linux64.tar.gz && \
    rm -rf ScaResolver-linux64.tar.gz && \
    chmod +x ScaResolver && \
    ls -la

ENV PATH="/opt/sca:${PATH}"

RUN python3 -m pip install --user virtualenv

# Copy European CxSca Datacenter configuration
#COPY "$CI_HOME/eu.Configuration.ini" /opt/sca/Configuration.ini
